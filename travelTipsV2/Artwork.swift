//
//  Artwork.swift
//  travelTips
//
//  Created by MDS on 27/02/2020.
//  Copyright © 2020 MDS. All rights reserved.
//

import Foundation
import MapKit
import Contacts
import SwiftyJSON

class Artwork: NSObject, MKAnnotation {
    let title: String?
    let nomadScore: Double?
    let locationName: String
    let coordinate: CLLocationCoordinate2D
    let image: String
    var markerTintColor: UIColor  {
        if Int(nomadScore!) >= 70 {
            return .green
        } else if Int(nomadScore!) >= 58 {
            return .yellow
        } else {
            return .red
        }
    }

    init(title: String, locationName: String, discipline: String, coordinate: CLLocationCoordinate2D, nomadScore: Double, image: String) {
        self.title = title
        self.nomadScore = nomadScore
        self.locationName = locationName
        self.coordinate = coordinate
        self.image = image
        super.init()
    }
    
    init?(json: JSON) {
        self.title = json["name"].string!
        self.nomadScore = Double(round(100 * json["nomad_score"].double!)/100)
        self.locationName = "nomad score : \(self.nomadScore ?? 0)"
        self.image = json["image"].string!
        if let latitude = json["latitude"].double, let longitude = json["longitude"].double {
            self.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        } else {
            self.coordinate = CLLocationCoordinate2D()
        }
    }
    
    //call without api bcs of limit excedeed
    init?(json: [Any]) {
        self.title = (json[2] as! String)
        self.nomadScore = Double(round(100 * Double(json[3] as! Double))/100)
        self.locationName = "nomad score : \(self.nomadScore ?? json[2])"
        self.image = (json[4] as! String)
        if let latitude = json[0] as? Double, let longitude = json[1] as? Double {
            self.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        } else {
            self.coordinate = CLLocationCoordinate2D()
        }
    }
    
    func mapItem() -> MKMapItem {
      let addressDict = [CNPostalAddressStreetKey: subtitle!]
      let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict)
      let mapItem = MKMapItem(placemark: placemark)
      mapItem.name = title
      return mapItem
    }
  
    var subtitle: String? {
        return locationName
    }
}
