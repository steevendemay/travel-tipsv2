//
//  CityDetailViewController.swift
//  travelTipsV2
//
//  Created by MDS on 28/02/2020.
//  Copyright © 2020 MDS. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Kingfisher
import Cosmos

class CityDetailViewController: UIViewController {
    @IBOutlet weak var safetyProgressView: CustomProgressView!
    @IBOutlet weak var funProgressView: CustomProgressView!
    @IBOutlet weak var nomadScoreView: UIView!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nomadScoreStar: CosmosView!
    let apiCityDetailUrl : String = "https://us-central1-traveltips-da7fd.cloudfunctions.net/webApi/api/v1/city/"
    var city : City? = nil
    override func viewDidLoad() {
        nomadScoreView.layer.cornerRadius = 10;
        nomadScoreView.layer.masksToBounds = true;
        nomadScoreView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        //if(city != nil){
            requestCity()
        //}else{
            //Rediriger si city est null
        //}
        super.viewDidLoad()
    }
    func requestCity(){
        let urlWithCityId = apiCityDetailUrl + "10"//String(city!._id)
        let url = URL(string: urlWithCityId)
        var request = URLRequest(url: url!)
        request.httpMethod = "get"
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            do {
                let json = try JSON(data : data!)
                print(json)
                self.city = City(_id: json["id"].int!)
                self.city!.fillWithJson(json: json)
                DispatchQueue.main.async {
                    self.buildView()
                }
            } catch let e {
                print("Error \(e)")
            }
        }
        task.resume()
    }
    func buildView(){
        countryLabel.text = city!._country
        nameLabel.text = city!._name
        let url = URL(string: city!._image!)
        imageView.kf.setImage(with: url)
        print((city!._nomad_score! / 100) * 5)
        nomadScoreStar.rating = (city!._nomad_score! / 100) * 5
    
        
        mapProgressBar(progressBar: funProgressView, progress: city!._fun_score!)
        mapProgressBar(progressBar: safetyProgressView, progress: city!._safety_score!)
    }
    func mapProgressBar(progressBar : CustomProgressView,progress : Double){
        progressBar.progress = Float(progress / 100)
        
    }
    
    class CustomProgressView: UIProgressView {
        override func layoutSubviews() {
            super.layoutSubviews()
            let maskLayerPath = UIBezierPath(roundedRect: bounds, cornerRadius: 4.0)
            let maskLayer = CAShapeLayer()
            maskLayer.frame = self.bounds
            maskLayer.path = maskLayerPath.cgPath
            layer.mask = maskLayer
        }
    }
}
