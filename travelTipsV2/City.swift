//
//  City.swift
//  travelTipsV2
//
//  Created by MDS on 28/02/2020.
//  Copyright © 2020 MDS. All rights reserved.
//

import Foundation
import SwiftyJSON

class City {
    var _id: Int
    var _name: String?
    var _country: String?
    var _continent: String?
    var _latitude: Double?
    var _longitude: Double?
    var _map_image: String?
    var _image: String?
    var _nomad_cost: Double?
    var _expat_cost: Double?
    var _family_cost: Double?
    var _local_cost: Double?
    var _studio_cost: Double?
    var _hotel_cost: Double?
    var _airbnb_cost: Double?
    var _meal_price: Double?
    var _coca_price: Double?
    var _beer_price: Double?
    var _coffee_price: Double?
    var _nomad_score: Double?
    var _fun_score: Double?
    var _safety_score: Double?
    var _life_quality_score: Double?
    var _walkability_score: Double?
    var _peace_score: Double?
    var _trafic_safety_score: Double?
    var _hospital_score: Double?
    var _happiness_score: Double?
    var _nightlife_score: Double?
    var _free_wifi_score: Double?
    var _places_to_work_score: Double?
    var _clim_score: Double?
    var _foreigner_friendly_score: Double?
    var _english_speaking_score: Double?
    var _speech_freedom_score: Double?
    var _racial_tolerence_score: Double?
    var _female_friendly_score: Double?
    var _lgbt_friendly_score: Double?
    var _startup_score: Double?
    var _avg_internet_speed: String?
    var _best_taxi_app_logo: String?
    var _best_taxi_app_name: String?
    var _best_taxi_app_link: String?
    var _best_wireless_carrier_logo: String?
    var _best_wireless_carrier_name: String?
    var _best_wireless_carrier_link: String?
    var _tipping: Bool?
    var _cashless: String?
    var _cashless_link: String?
    var _best_coworking_space_name: String?
    var _best_coworking_space_link: String?
    var _best_coffee_place_link: String?
    var _best_coffee_place_name: String?
    var _safe_water_link: String?
    var _safe_water: String?
    var _apartment_listing_name: String?
    var _apartment_listing_link: String?
    var _apartment_listing_logo: String?
    
    init(_id: Int, _name: String, _country: String, _continent: String, _latitude: Double, _longitude: Double, _map_image: String, _image: String, _nomad_cost: Double, _expat_cost: Double, _family_cost: Double, _local_cost: Double, _studio_cost: Double, _hotel_cost: Double, _airbnb_cost: Double, _meal_price: Double, _coca_price: Double, _beer_price: Double, _coffee_price: Double, _nomad_score: Double, _fun_score: Double, _safety_score: Double, _life_quality_score: Double, _walkability_score: Double, _peace_score: Double, _trafic_safety_score: Double, _hospital_score: Double, _happiness_score: Double, _nightlife_score: Double, _free_wifi_score: Double, _places_to_work_score: Double, _clim_score: Double, _foreigner_friendly_score: Double, _english_speaking_score: Double, _speech_freedom_score: Double, _racial_tolerence_score: Double, _female_friendly_score: Double, _lgbt_friendly_score: Double, _startup_score: Double, _avg_internet_speed: String, _best_taxi_app_logo: String, _best_taxi_app_name: String, _best_taxi_app_link: String, _best_wireless_carrier_logo: String, _best_wireless_carrier_name: String, _best_wireless_carrier_link: String, _tipping: Bool, _cashless: String, _cashless_link: String, _best_coworking_space_name: String, _best_coworking_space_link: String, _best_coffee_place_link: String, _best_coffee_place_name: String, _safe_water_link: String, _safe_water: String, _apartment_listing_name: String, _apartment_listing_link: String, _apartment_listing_logo: String) {
        self._id = _id
        self._name = _name
        self._country = _country
        self._continent = _continent
        self._latitude = _latitude
        self._longitude = _longitude
        self._map_image = _map_image
        self._image = _image
        self._nomad_cost = _nomad_cost
        self._expat_cost = _expat_cost
        self._family_cost = _family_cost
        self._local_cost = _local_cost
        self._studio_cost = _studio_cost
        self._hotel_cost = _hotel_cost
        self._airbnb_cost = _airbnb_cost
        self._meal_price = _meal_price
        self._coca_price = _coca_price
        self._beer_price = _beer_price
        self._coffee_price = _coffee_price
        self._nomad_score = _nomad_score
        self._fun_score = _fun_score
        self._safety_score = _safety_score
        self._life_quality_score = _life_quality_score
        self._walkability_score = _walkability_score
        self._peace_score = _peace_score
        self._trafic_safety_score = _trafic_safety_score
        self._hospital_score = _hospital_score
        self._happiness_score = _happiness_score
        self._nightlife_score = _nightlife_score
        self._free_wifi_score = _free_wifi_score
        self._places_to_work_score = _places_to_work_score
        self._clim_score = _clim_score
        self._foreigner_friendly_score = _foreigner_friendly_score
        self._english_speaking_score = _english_speaking_score
        self._speech_freedom_score = _speech_freedom_score
        self._racial_tolerence_score = _racial_tolerence_score
        self._female_friendly_score = _female_friendly_score
        self._lgbt_friendly_score = _lgbt_friendly_score
        self._startup_score = _startup_score
        self._avg_internet_speed = _avg_internet_speed
        self._best_taxi_app_logo = _best_taxi_app_logo
        self._best_taxi_app_name = _best_taxi_app_name
        self._best_taxi_app_link = _best_taxi_app_link
        self._best_wireless_carrier_logo = _best_wireless_carrier_logo
        self._best_wireless_carrier_name = _best_wireless_carrier_name
        self._best_wireless_carrier_link = _best_wireless_carrier_link
        self._tipping = _tipping
        self._cashless = _cashless
        self._cashless_link = _cashless_link
        self._best_coworking_space_name = _best_coworking_space_name
        self._best_coworking_space_link = _best_coworking_space_link
        self._best_coffee_place_link = _best_coffee_place_link
        self._best_coffee_place_name = _best_coffee_place_name
        self._safe_water_link = _safe_water_link
        self._safe_water = _safe_water
        self._apartment_listing_name = _apartment_listing_name
        self._apartment_listing_link = _apartment_listing_link
        self._apartment_listing_logo = _apartment_listing_logo
    }
    
    init(_id: Int, _latitude: Double, _longitude: Double, _name: String, _image: String, _nomadScore: Double) {
        self._id = _id
        self._latitude = _latitude
        self._longitude = _longitude
        self._name = _name
        self._image = _image
        self._nomad_score = _nomadScore
    }
    
    init(_id: Int, _name: String, _image: String, _nomadScore: Double) {
        self._id = _id
        self._name = _name
        self._image = _image
        self._nomad_score = _nomadScore
    }
    init(_id: Int) {
        self._id = _id
    }
    func fillWithJson(json : JSON){
        self._id = json["id"].int!
        self._name = json["name"].string!
        self._country = json["country"].string!
        self._continent = json["continent"].string!
        self._latitude = json["latitude"].double!
        self._longitude = json["longitude"].double!
        self._map_image = json["continent"].string!
        self._image = json["image"].string!
        self._nomad_cost = json["nomad_cost"].double!
        self._expat_cost = json["expat_cost"].double!
        self._family_cost = json["family_cost"].double!
        self._local_cost = json["local_cost"].double!
        self._studio_cost = json["studio_cost"].double!
        self._hotel_cost = json["hotel_cost"].double!
        self._airbnb_cost = json["airbnb_cost"].double!
        self._meal_price = json["meal_price"].double!
        self._coca_price = json["coca_price"].double!
        self._beer_price = json["beer_price"].double!
        self._coffee_price = json["coffee_price"].double!
        self._nomad_score = json["nomad_score"].double!
        self._fun_score = json["fun_score"].double!
        self._safety_score = json["safety_score"].double!
        self._life_quality_score = json["life_quality_score"].double!
        self._walkability_score = json["walkability_score"].double!
        self._peace_score = json["peace_score"].double!
        self._trafic_safety_score = json["trafic_safety_score"].double!
        self._hospital_score = json["hospital_score"].double!
        self._happiness_score = json["happiness_score"].double!
        self._nightlife_score = json["nightlife_score"].double!
        self._free_wifi_score = json["free_wifi_score"].double!
        self._places_to_work_score = json["places_to_work_score"].double!
        self._clim_score = json["clim_score"].double!
        self._foreigner_friendly_score = json["foreigner_friendly_score"].double!
        self._english_speaking_score = json["english_speaking_score"].double!
        self._speech_freedom_score = json["speech_freedom_score"].double!
        self._racial_tolerence_score = json["racial_tolerence_score"].double!
        self._female_friendly_score = json["female_friendly_score"].double!
        self._lgbt_friendly_score = json["lgbt_friendly_score"].double!
        self._startup_score = json["startup_score"].double!
        self._avg_internet_speed = json["avg_internet_speed"].string!
        self._best_taxi_app_logo = json["best_taxi_app_logo"].string!
        self._best_taxi_app_name = json["best_taxi_app_name"].string!
        self._best_taxi_app_link = json["best_taxi_app_link"].string!
        self._best_wireless_carrier_logo = json["best_wireless_carrier_logo"].string!
        self._best_wireless_carrier_name = json["best_wireless_carrier_name"].string!
        self._best_wireless_carrier_link = json["best_wireless_carrier_link"].string!
        self._tipping = json["tipping"].bool
        self._cashless = json["cashless"].string!
        self._cashless_link = json["cashless_link"].string!
        self._best_coworking_space_name = json["best_coworking_space_name"].string!
        self._best_coworking_space_link = json["best_coworking_space_link"].string
        self._best_coffee_place_link = json["best_coffee_place_link"].string
        self._best_coffee_place_name = json["best_coffee_place_name"].string
        self._safe_water_link = json["safe_water_link"].string!
        self._safe_water = json["safe_water"].string!
        self._apartment_listing_name = json["apartment_listing_name"].string!
        self._apartment_listing_link = json["apartment_listing_link"].string!
        self._apartment_listing_logo = json["apartment_listing_logo"].string!
        
    }
}
