//
//  CitiesViewController.swift
//  travelTipsV2
//
//  Created by MDS on 27/02/2020.
//  Copyright © 2020 MDS. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Kingfisher

class CitiesViewController: UIViewController, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchCityTextField: UITextField! {
       didSet {
            searchCityTextField.tintColor = UIColor(hexFromString: "118C76")
            searchCityTextField.setIcon(UIImage(named: "search")!)
       }
    }
    
    @IBOutlet weak var constraintTopTableView: NSLayoutConstraint!
    
    
    var cities: [City] = []
    var listFilters = ["Nomad Score", "Safety", "Fun"]
    var filter = "nomad_score"
    var tableTemp: [CGFloat] = []
    var btnSort: UIButton!
    var sortActive = "desc"

    var scView:UIScrollView!
    let buttonPadding:CGFloat = 10
    var xOffset:CGFloat = 10
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 400
        
        getCities(filter: self.filter)
        generateListFilter()
        
        let btnSelected: UIButton = view?.viewWithTag(31) as! UIButton
        btnSelected.setTitleColor(UIColor.white, for: UIControl.State.normal)
        btnSelected.backgroundColor = UIColor(hexFromString: "118C76")
    }
    

    func generateListFilter() {
        
        self.btnSort = UIButton(frame: CGRect(x: 0, y: self.searchCityTextField.frame.height + 65, width: 40, height: 50))
        self.btnSort.setTitleColor(UIColor(hexFromString: "118C76"), for: UIControl.State.normal)

        let icon = UIImage(named: "desc-arrow")!
        self.btnSort.setImage(icon, for: .normal)
        self.btnSort.imageView?.contentMode = .scaleAspectFit
        self.btnSort.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.btnSort.addTarget(self, action: #selector(handleClickSort), for:.touchUpInside)

        scView = UIScrollView(frame: CGRect(x: 40, y: self.searchCityTextField.frame.height + 65, width: view.bounds.width, height: 50))
        scView.showsHorizontalScrollIndicator = false

        view.addSubview(self.btnSort)
        view.addSubview(scView)

        scView.translatesAutoresizingMaskIntoConstraints = false

        var tag = 30
        for filter in self.listFilters {
            
            tag = tag + 1
            let button = UIButton()
            
            button.backgroundColor = UIColor.clear
            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
            button.setTitleColor(UIColor(hexFromString: "118C76"), for: UIControl.State.normal)
            
            button.setTitle("\(filter)", for: .normal)
            button.addTarget(self, action: #selector(handleClickFilter(_:)), for:.touchUpInside)
            button.tag = tag
            button.layer.cornerRadius = 10
            button.layer.borderWidth = 1
            button.layer.borderColor = UIColor(hexFromString: "118C76").cgColor
            
   
            button.frame = CGRect(x: xOffset, y: CGFloat(buttonPadding), width: 135, height: 30)

            xOffset = xOffset + CGFloat(buttonPadding) + button.frame.size.width
            scView.addSubview(button)
        }
        scView.contentSize = CGSize(width: xOffset, height: scView.frame.height)
    }
    
    @objc func handleClickFilter(_ sender : UIButton) {
        for i in 31..<34 {
            let btn: UIButton = view?.viewWithTag(i) as! UIButton
            btn.backgroundColor = UIColor.clear
            btn.setTitleColor(UIColor(hexFromString: "118C76"), for: UIControl.State.normal)
        }
        let btnSelected: UIButton = view?.viewWithTag(sender.tag) as! UIButton
        btnSelected.setTitleColor(UIColor.white, for: UIControl.State.normal)
        btnSelected.backgroundColor = UIColor(hexFromString: "118C76")
        
        switch sender.tag {
            case 31 :getCities(filter: "nomad_score")
            case 32 : getCities(filter: "safety_score")
            case 33: getCities(filter: "fun_score")
            default:getCities(filter: "nomad_score")
        }
    }
    

    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let indexpath = self.tableView.indexPathsForVisibleRows?.last
        if (indexpath != nil) {
            if(indexpath?.last == self.cities.count - 1){
                getCities(filter: self.filter)
            }
        }
        
        if scrollView.panGestureRecognizer.translation(in: scrollView).y < 0{
            //scrolling down
            UIView.animate(withDuration: 0.3, animations: {
                self.scView.alpha = 0
                self.searchCityTextField.alpha = 0
                self.btnSort.alpha = 0
                self.constraintTopTableView.constant = 0
            }) { (finished) in
            }
        }else{
            //scrolling up
            UIView.animate(withDuration: 0.5, animations: {
                self.scView.alpha = 1
                self.searchCityTextField.alpha = 1
                self.btnSort.alpha = 1
            }) { (finished) in
                self.constraintTopTableView.constant = 125
            }
        }
    }
    
    @objc func handleClickSort() {
        self.cities = []
        if self.sortActive == "desc" {
            let icon = UIImage(named: "asc-arrow")!
            self.btnSort.setImage(icon, for: .normal)
            self.btnSort.imageView?.contentMode = .scaleAspectFit
            self.btnSort.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            self.sortActive = "asc"
            self.getCities(filter: self.filter)
        } else {
            let icon = UIImage(named: "desc-arrow")!
            self.btnSort.setImage(icon, for: .normal)
            self.btnSort.imageView?.contentMode = .scaleAspectFit
            self.btnSort.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            self.sortActive = "desc"
            self.getCities(filter: self.filter)
        }
    }
    
    
    func getCities(filter: String) {
        if(self.filter != filter) {
            self.filter = filter
            self.cities = []
        }
        let stringURL = "https://us-central1-traveltips-da7fd.cloudfunctions.net/webApi/api/v1/city?limit=20&offset=" + String(self.cities.count) + "&orderby=" + self.filter + "&ordertype=" + self.sortActive
        
        let url = URL(string: stringURL)
        var request = URLRequest(url: url!)
        request.httpMethod = "get"
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            do {
                let json = try JSON(data: data!)
                let citiesJSON: JSON = json["cities"]
                for (_,city) in citiesJSON {
                    let city = City(_id: city["id"].int!, _name: city["name"].string!, _image: city["image"].string!, _nomadScore: city["nomad_score"].double!)
                    self.cities.append(city)
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            } catch {
                print("error")
            }
        }.resume()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cities.count
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        //city selected
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        let imageView: UIImageView = cell?.viewWithTag(1) as! UIImageView
        let nameCity: UILabel = cell?.viewWithTag(2) as! UILabel
        let nomadScore: UILabel = cell?.viewWithTag(3) as! UILabel
        let view: UIView = cell?.viewWithTag(5) as! UIView

        view.layer.cornerRadius = 10

        
        let imageURL = self.cities[indexPath.row]._image
        let imageURLKF = URL(string: imageURL!)
        imageView.kf.setImage(with: imageURLKF)
        imageView.alpha = 0.85
        
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 1
     
        nameCity.layer.shadowColor = UIColor.black.cgColor
        nameCity.layer.shadowOpacity = 1
        
        
        nameCity.text = self.cities[indexPath.row]._name
        let nomadStr = String(format: "%.2f", self.cities[indexPath.row]._nomad_score!)
        nomadScore.text = nomadStr + " % "
        
        return cell!
    }
        
    @IBAction func handleChangedSearchCity(_ sender: Any) {
        print(searchCityTextField.text!)
    }
}

