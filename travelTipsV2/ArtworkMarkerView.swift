//
//  ArtworkView.swift
//  travelTipsV2
//
//  Created by MDS on 28/02/2020.
//  Copyright © 2020 MDS. All rights reserved.
//

import MapKit
import Kingfisher

class ArtworkMarkerView: MKMarkerAnnotationView {
    override var annotation: MKAnnotation? {
        willSet {
            guard let artwork = newValue as? Artwork else { return }
            canShowCallout = true
            calloutOffset = CGPoint(x: -5, y: 5)
            let mapsButton = UIButton(frame: CGRect(origin: CGPoint.zero,size: CGSize(width: 40, height: 40)))
            
            let imageURLKF = URL(string: artwork.image)
            mapsButton.kf.setBackgroundImage(with: imageURLKF, for: UIControl.State())
            
            rightCalloutAccessoryView = mapsButton
            markerTintColor = artwork.markerTintColor
            glyphText = String(format:"%.1f", artwork.nomadScore!)
        }
    }
}
