//
//  MapViewController.swift
//  travelTipsV2
//
//  Created by MDS on 27/02/2020.
//  Copyright © 2020 MDS. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import SwiftyJSON

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    var locationManager = CLLocationManager()
    var userPosition: CLLocation?
    var artworks: [Artwork] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        mapView.showsUserLocation = true
        
        mapView.register(ArtworkMarkerView.self,
        forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
        setupMap(coordonnee: CLLocationCoordinate2D(latitude: 48.048503, longitude: -1.742261))
        loadCityMap()
    }
    
    //centrer la carte
    func setupMap(coordonnee: CLLocationCoordinate2D) {
        let span = MKCoordinateSpan(latitudeDelta: 15, longitudeDelta: 15)
        let region = MKCoordinateRegion(center: coordonnee, span: span)
        mapView.setRegion(region, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,
     calloutAccessoryControlTapped control: UIControl) {
      let location = view.annotation as! Artwork
      let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
      location.mapItem().openInMaps(launchOptions: launchOptions)
        
    }
    
    
    func loadCityMap() {
        // Decommenter pour utiliser l'api
        //getApiCitiesJson()
        // Commenter pour utiliser l'api
        getJsonFileCities()

    }
    
    func getApiCitiesJson() {
        let url = URL(string: "https://us-central1-traveltips-da7fd.cloudfunctions.net/webApi/api/v1/city/map")
        var request = URLRequest(url: url!)
        request.httpMethod = "get"
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            do {
                let json = try JSON(data: data!)

                let citiesJSON: JSON = json["city_filters"]

                for (_,city) in citiesJSON {
                  let artwork = Artwork(json: city)
                  self.artworks.append(artwork!)
                }
              DispatchQueue.main.async {
                  self.mapView.addAnnotations(self.artworks)
              }
            } catch {
                print("error")
            }
        }.resume()
    }
    
    // Fonction rajouté pour lire les données dans le fichier json, seulement si la limitation de requête de l'api est atteinte
    func getJsonFileCities() {
        if let path = Bundle.main.path(forResource: "Cities", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                let jsonObj = try JSON(data: data)
                let citiesJSON: JSON = jsonObj["city_filters"]
                for (_,city) in citiesJSON {
                    let artwork = Artwork(json: city)
                    self.artworks.append(artwork!)
                  }
                DispatchQueue.main.async {
                    self.mapView.addAnnotations(self.artworks)
                }
            } catch let error {
                print("parse error: \(error.localizedDescription)")
            }
        } else {
            print("Invalid filename/path.")
        }
    }
    
}
