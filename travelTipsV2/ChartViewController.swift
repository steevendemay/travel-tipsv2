import UIKit
import SwiftyJSON
import Charts

class ChartViewController: UIViewController {
    
    @IBOutlet weak var barChartLifeCost: BarChartView!
    @IBOutlet weak var barChartLabel: UILabel!
    
    var continents: JSON = []
    var continentsList = [String]()
    var countries: JSON = []
    var cities: JSON = []

    override func viewDidLoad() {
        super.viewDidLoad()
        print("test")
        requestLifeCostContinent()
        
    }

    func requestLifeCostContinent() {
        let url = URL(string: "https://us-central1-traveltips-da7fd.cloudfunctions.net/webApi/api/v1/city/lifeCostContinent")
        var request = URLRequest(url: url!)
        request.httpMethod = "get"
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            do {
                let json = try JSON(data: data!)
                self.continents = json["data"]

                self.continents.dictionaryObject?.removeValue(forKey: "Oceania")
                self.continents.dictionaryObject?.removeValue(forKey: "Middle East")
                self.continents.dictionaryObject?.removeValue(forKey: "Antarctica")
                for (key, _) in self.continents {
                         self.continentsList.append(key)
                }
                DispatchQueue.main.async {
                    self.barChartContinents()
                }
                print(self.continentsList)
            } catch _ {
                print("Error \(String(describing: error))")
            }
        }.resume()
    }

//    func requestLivingCostPopulation() {
//        let url = URL(string: "")
//        var request = URLRequest(url: url!)
//        request.httpMethod = "get"
//        URLSession.shared.dataTask(with: request) {
//            (data, response, error) in
//            do {
//                let json = try JSON(data: data!)
//                self.cities = json["data"]
//                print(self.cities)
//            } catch _ {
//                print("Error \(String(describing: error))")
//            }
//        }.resume()
//    }
//
//    func requestVisitorsByCountry() {
//        let url = URL(string: "")
//        var request = URLRequest(url: url!)
//        request.httpMethod = "get"
//        URLSession.shared.dataTask(with: request) {
//            (data, response, error) in
//            do {
//                let json = try JSON(data: data!)
//                self.countries = json["data"]
//                print(self.countries)
//            } catch _ {
//                print("Error \(String(describing: error))")
//            }
//        }.resume()
//    }

    func barChartContinents() {
        var entries = [BarChartDataEntry]()
        var cpt = 0

        for (_, value) in self.continents {
            entries.append(BarChartDataEntry(x:Double(cpt+1),y:value.double!))
            cpt += 1
        }
        let dataSet = BarChartDataSet(entries: entries,label: "Average Cost of living for a local")
        dataSet.colors = [UIColor(hexFromString: "118C76")]
        let data = BarChartData(dataSet:dataSet)
        barChartLifeCost.xAxis.drawGridLinesEnabled = false
//        barChartLifeCost.xAxis.valueFormatter = IndexAxisValueFormatter(values: self.continentsList)
        barChartLifeCost.xAxis.valueFormatter = self
        barChartLifeCost.xAxis.labelPosition = XAxis.LabelPosition.bottom
        barChartLifeCost.doubleTapToZoomEnabled = false
        

        barChartLifeCost.data = data
    }
//
//    func scatterPlotCostLivingPerPopulation() {
//
//    }
    @IBOutlet weak var segmentedControl: UISegmentedControl!

    @IBAction func selectChart(_ sender: Any) {
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            barChartLifeCost.isHidden = false
            barChartLabel.isHidden = false
//            requestLifeCostContinent()
        case 1:
            barChartLifeCost.isHidden = true
            barChartLabel.isHidden = true
//            requestLivingCostPopulation()
        case 2:
            barChartLifeCost.isHidden = true
            barChartLabel.isHidden = true
//            requestVisitorsByCountry()
        default:
            break;
        }
    }
    
}

extension ChartViewController: IAxisValueFormatter{
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return continentsList[ Int(value) % continentsList.count]
    }
}
